package collectEnvVar

import (
	"encoding/json"
	"os"
	"strings"
)

func EnvVar() map[string]interface{} {
	var result map[string]interface{}
	env := os.Environ()
	envVar := make(map[string]interface{})
	for _, value := range env {
		envPair := strings.SplitN(value, "=", 2)
		if envPair[0] == "PIPELINE" {
			text := envPair[1][1 : len(envPair[1])-1]
			text = strings.ReplaceAll(text, "\\n", "")
			text = strings.ReplaceAll(text, "\\", "")
			err := json.Unmarshal([]byte(text), &result)
			if err != nil {
				panic(err)
			}
			envVar["PIPELINE"] = result
		} else {
			envVar[envPair[0]] = envPair[1]
		}
	}
	return envVar
}
